# ESL
React - es2015 project BABEL + BROWSERIFY

## Features ###
* npm scripts
* es2015
* Lint ESLINT
* Unit testing TAPE
* BrowserSync
* React Storybook
* Express

## Clone the repo & install
```
git clone 
cd ./esl-test
npm install
```
## Usage
* run npm start
* launch your server at http://localhost:8000

## Tips
* Example of component.
* Unit test example with tape.
* Example of react Storybook, with dummy component.

## Scripts

The `package.json` file comes with the following scripts

`npm run <name of script>`

`test`: run provided unit tests.

`coverage`: add coverage when running tests.

`coverall`: run coverage tasks.

`lint`: lint all babel code.

`browsersync`: launch a dev server(http://localhost:3000) providing the app at ./build, start watching files at ./src. proxy to port:4000.

`build`: generates the compiled app at ./build directory.

`dev`:  runs a dev console that reports lint and unit tests and start watching for changes at *.js files, perform build tasks.

`start`: runs server at port:8080 provide files place at ./public.

`storybook`:  launch a server at port 9001.

`dist`: generates a distribution version, placed at ./dist

`prepublish`: perform prepublish:create and dist task, copy files from ./dist to ./server/public.

`check`: check outdated npm modules.

`update`: update outdated npm modules.

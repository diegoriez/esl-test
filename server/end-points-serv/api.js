var path = require('path');
var fs = require('fs');

module.exports = function(app, opts) {

    app.get('/api/:id/data', function(req, res) {


        var file = path.format({
            dir: opts.data,
            base: path.join(req.params.id, 'data.json')
        });

        res
            .status(200)
            .send(opts.getJson(file));
    });


    app.post('/api/:id/data', function(req, res) {

        var file = path.format({
            dir: 'data',
            base: path.join(req.params.id, 'data.json')
        });

        fs.readFile(file, 'utf8', function(err, data) {

            if (err) {
                throw err;
            }

            var obj = JSON.parse(data);
            var days = obj.data.days.slice(0);
            var ln = days.length;
            var i = 0;

            while (i < ln) {

                if (days[i].id === req.body.id) {

                    days[i] = Object.assign(days[i], req.body);

                    break;
                }

                i += 1;
            }

            obj.data.days = days;
            var updated = JSON.stringify(obj);

            fs.writeFile(file, updated, function(err) {

                if (err) {
                    throw err;
                }

                console.log('written');

                return;
            });
        });

        res.sendStatus(200);
    });

};

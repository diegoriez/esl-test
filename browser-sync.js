var bs = require('browser-sync').create(),
    proxy = require('proxy-middleware'),
    url = require('url'),
    bs1 = require("browser-sync").create('Server1');

var proxy_options = function(value) {
    var proxyOptions = url.parse(value);
    proxyOptions.route = '/api';
    return proxyOptions;
};

bs.watch('src/**/*').on('change', bs.reload);
bs.init({
    server: {
        name: 'dev',
        baseDir: './build',
        middleware: [proxy(proxy_options('http://localhost:8080/api'))]

    },
    port: 3000,
    ui: {
        port: 3012
    },
    reloadDelay: 3000
});

bs1.init({
    server: {
        name: 'dev1',
        baseDir: './build/markdown'
    },
    port: 3001,
    ui: {
        port: 3014
    },
    reloadDelay: 3000
});

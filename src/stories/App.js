import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { getDayList, getDayListSchema, getweekStruturePlanning } from '../tests/fixtures';
import WeekStructurePlanningSelector from '../scripts/components/WeekStructurePlanningSelector';
import Comment from '../scripts/components/Comment';
import WeekPlannerTable from '../scripts/components/WeekPlannerTable';
import ActionSubmitCancel from '../scripts/components/ActionSubmitCancel';
import EditDay from '../scripts/components/EditDay';

const $ = React.createElement;

storiesOf('ESL', module)
    .add('WeekStructurePlanningSelector', () => {

        const props = {
            data: getDayList(),
            title: 'First Day'
        };

        return $(WeekStructurePlanningSelector(React), props);
    })
    .add('Comment', () => {

        const props = {
            commentary: 'Lorem ipsum dolor sit amet.',
            handleChange: action('handleChange was invoked')
        };

        return $(Comment, props);

    })
    .add('weekPlannerTable', () => {

        const props = {
            days: getDayListSchema().entities.days,
            result: getDayListSchema().result,
            periods: getweekStruturePlanning().data.beginningOfPeriod
        };

        return $(WeekPlannerTable(React), props);
    })
    .add('ActionSubmitCancel', () => {

        return $(ActionSubmitCancel);
    })
    .add('EditDay', () => {

        const props = {
            comment: 'Lorem.',
            display: 'block',
            handleCancel: action('handleCancel invoked'),
            editDayId: { id: '3', period: 'moorning' },
            handleSave: (fn) =>
                () => {
                    action(`handleSave invoked textInput value ---> ${JSON.stringify(fn())}`)();
                }
        };
        return $(EditDay, props);
    });

import { connect } from 'react-redux';
import Comment from 'scripts/components/Comment';
import { commentUpdated } from 'scripts/actions';

const mapStateToProps = state => ({
    commentary: state.comment
});

const mapDispatchToProps = (dispatch) => ({
    handleChange: ({ target }) => {
        dispatch(commentUpdated(target.value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Comment);

import { connect } from 'react-redux';
import WeekPlannerTable from 'scripts/components/WeekPlannerTable';

const mapStateToProps = state => ({
    days: state.days.entities.days,
    result: state.days.result,
    periods: state.weekStruturePlanning.beginningOfPeriod
});

const comp = React => connect(
    mapStateToProps,
)(WeekPlannerTable(React));

export default comp;

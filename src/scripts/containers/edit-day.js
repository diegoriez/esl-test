import { compose, curry, path, prop, isNil } from 'ramda';
import { connect } from 'react-redux';
import EditDay from 'scripts/components/EditDay';
import { editDay, weekDayUpdated } from 'scripts/actions';

const getComment = ({ editDayId, days }) => {

    if (!editDayId.id) {
        return void 0;
    }

    const id = prop('id', editDayId);
    const period = prop('period', editDayId);
    const day = path(['entities', 'days'], days);

    return path([id, period], day);
};

const mapStateToProps = state => ({
    comment: getComment(state),
    editDayId: prop('editDayId', state),
    display: isNil(path(['editDayId', 'id'], state)) ? 'none' : 'block'
});

const mapDispatchToProps = (dispatch) => ({
    handleCancel: () => {
        dispatch(editDay());
    },
    handleSave: curry((fn) => compose(dispatch, weekDayUpdated, fn))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditDay);

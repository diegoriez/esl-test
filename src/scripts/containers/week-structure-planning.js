import { connect } from 'react-redux';
import WeekStructurePlanningHeader from 'scripts/components/WeekStructurePlanningHeader';

const mapStateToProps = state => ({
    data: state.weekStruturePlanning
});

const comp = React => connect(
    mapStateToProps,
)(WeekStructurePlanningHeader(React));

export default comp;

import { normalize, schema } from 'normalizr';

// Normalizes deeply nested JSON API
export const normalizeWeekDaysData = (value) => {
    const daysSchema = new schema.Entity('days');
    const daysListSchema = new schema.Array(daysSchema);
    return (normalize(value, daysListSchema));

};

import delegate from 'delegate';
import { $qs } from 'scripts/helpers';
import { editDay } from 'scripts/actions';

export default (dispatch) => {
    delegate($qs('#root'), '#week-planner-table td', 'click', ({ delegateTarget }) => {
        dispatch(editDay({
            period: delegateTarget.dataset.period,
            id: delegateTarget.id
        }));

    }, false);
};

import fetch from 'isomorphic-fetch';
import { Maybe } from 'ramda-fantasy';
import { compose, curry, ifElse, isNil, path, prop } from 'ramda';
import { normalizeWeekDaysData } from 'scripts/helpers/received-data';
import {
    weekStructureDataReceived,
    weekPlanDataReceived,
    commentUpdated
} from 'scripts/actions';

const Just = Maybe.Just;
const Nothing = Maybe.Nothing;
const fmap = curry((fn, m) => m.map(fn));
const fchain = curry((fn, m) => m.chain(fn));

const fetchData = ({
        left = (a) => a,
        right = (b) => b,
        url = '',
        params = {}
    }) =>

    fetch(url, params)
    .then((value) => value.status !== 200 ? false : value.json())
    .then((value) => {
        const action = !value ? left : right;
        action(value);
    })
    .catch((error) => {
        throw error.message;
    });

export const fetchWeekStructureData = (dispatch) => {
    fetchData({
        url: 'api/week-structure/data',
        left: () => console.log('week structure data not found'),
        right: compose(
            fmap(compose(dispatch, weekStructureDataReceived)),
            fchain(ifElse(isNil, Nothing, Just)),
            Just,
            prop('data')
        )
    });
};

export const fetchWeekDaysDataPlanning = (dispatch) => {

    const weekDays = compose(
        fmap(compose(
            dispatch,
            weekPlanDataReceived,
            normalizeWeekDaysData)),
        fchain(ifElse(isNil, Nothing, Just)),
        Just,
        path(['data', 'days'])
    );

    const comment = compose(
        fmap(compose(dispatch, commentUpdated)),
        fchain(ifElse(isNil, Nothing, Just)),
        Just,
        path(['data', 'comment'])
    );

    fetchData({
        url: 'api/week-table/data',
        left: () => console.log('week plan data not found'),
        right: (value) => {
            weekDays(value);
            comment(value);
        }
    });
};

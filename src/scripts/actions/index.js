import { isNil } from 'ramda';

export const WEEK_STRUCTURE_PLANNING_DATA_RECEIVED = 'week.structure.planning.received';
export const WEEK_PLAN_DATA_RECEIVED = 'week.plan.data.received';
export const WEEK_DAY_UPDATED = 'week.day.updated';
export const WEEK_DAY_DELETED = 'delete.week.day';
export const COMMENT_UPDATED = 'comment.updated';
export const EDIT_DAY = 'edit.day';


export const weekStructureDataReceived = (value) => ({
    type: WEEK_STRUCTURE_PLANNING_DATA_RECEIVED,
    payload: value
});

export const commentUpdated = (value) => ({
    type: COMMENT_UPDATED,
    payload: value
});

export const weekPlanDataReceived = (value) => ({
    type: WEEK_PLAN_DATA_RECEIVED,
    payload: value
});

export const weekDayUpdated = (value) => ({
    type: WEEK_DAY_UPDATED,
    payload: value
});

export const weekDayDeleted = (value) => ({
    type: WEEK_DAY_DELETED,
    payload: value
});

export const editDay = (value) => ({
    type: EDIT_DAY,
    payload: isNil(value) ? {} : value
});

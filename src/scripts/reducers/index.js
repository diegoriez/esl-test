import { combineReducers } from 'redux';
import {
    append,
    contains,
    lensPath,
    merge,
    set,
    view
} from 'ramda';

import {
    WEEK_STRUCTURE_PLANNING_DATA_RECEIVED,
    WEEK_PLAN_DATA_RECEIVED,
    WEEK_DAY_UPDATED,
    COMMENT_UPDATED,
    WEEK_DAY_DELETED,
    EDIT_DAY
} from 'scripts/actions';

// ------------ REDUCERS
export const weekStruturePlanning = (state = {}, action) => {

    const actions = {
        [WEEK_STRUCTURE_PLANNING_DATA_RECEIVED]: () => Object.assign({}, action.payload),
        DEFAULT: () => state
    };

    const actionType = actions[action.type] || actions.DEFAULT;
    return actionType();
};

export const comment = (state = '', action) => {

    const actions = {
        [COMMENT_UPDATED]: () => action.payload,
        DEFAULT: () => state
    };

    const actionType = actions[action.type] || actions.DEFAULT;
    return actionType();
};

export const daysDataUpdated = (state = {}, action) => {

    const actions = {

        [WEEK_PLAN_DATA_RECEIVED]: () => action.payload,

        [WEEK_DAY_UPDATED]: () => {
            const dayLens = lensPath(['entities', 'days', action.payload.id]);
            const merged = merge(view(dayLens, state), action.payload);
            return set(dayLens, merged, state);
        },

        [WEEK_DAY_DELETED]: () => {
            const dayLens = lensPath(['entities', 'days', action.payload]);
            return set(dayLens, {}, state);
        },

        DEFAULT: () => state
    };

    const actionType = actions[action.type] || actions.DEFAULT;
    return actionType();
};

export const updatedDaysIds = (state = [], action) => {

    const actions = {
        [WEEK_DAY_UPDATED]: () => contains(action.payload.id, state) ? state : append(action.payload.id, state),
        [WEEK_DAY_DELETED]: () => contains(action.payload, state) ? state : append(action.payload, state),
        DEFAULT: () => state
    };

    const actionType = actions[action.type] || actions.DEFAULT;
    return actionType();
};

export const editDayId = (state = {}, action) => {

    const actions = {
        [EDIT_DAY]: () => Object.assign({}, action.payload),
        [WEEK_DAY_UPDATED]: () => ({ id: null, period: null }),
        DEFAULT: () => state
    };

    const actionType = actions[action.type] || actions.DEFAULT;
    return actionType();
};

export const rootReducer = combineReducers({
    comment,
    updatedDaysIds,
    weekStruturePlanning,
    editDayId,
    days: daysDataUpdated
});

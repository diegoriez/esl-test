export default React => {

    const $ = React.createElement;

    const {
        array
    } = React.PropTypes;

    const createTheadChildren = (days = {}, result = []) =>
        result.map((item, idx) =>
            $('th', { key: `day_${idx}` }, days[item].label));


    const createTbodyTrChildren = (days = {}, result = [], periodLabel = '') =>
        result.map((item, idx) =>
            $('td', { 'id': item, 'data-period': periodLabel, key: `td_${idx}` }, days[item][periodLabel])
        );

    const createTbodyChildren = (days = {}, result = [], periods = []) =>
        periods.map((item, idx) =>
            $('tr', { key: `per_${idx}` },
                $('td', {}, item.label),
                createTbodyTrChildren(days, result, item.label)
            ));

    const comp = ({ days, result, periods }) =>
        ($('table', { id: 'week-planner-table', className: 'week-schedule' },
            $('thead', {},
                $('tr', {},
                    $('th', {}),
                    createTheadChildren(days, result)
                )
            ),
            $('tbody', {},
                createTbodyChildren(days, result, periods)
            )
        ));

    comp.PropTypes = {
        periods: array.isRequired,
        days: array.isRequired
    };

    return comp;

};

 import React from 'react';

 const {
     string,
     func
 } = React.PropTypes;

 class Comment extends React.Component {

     constructor(props) {
         super(props);
     }

     shouldComponentUpdate(nextProps) {
         return (this.props.commentary !== nextProps.commentary);
     }

     render() {

         const $ = React.createElement;
         const { commentary, handleChange } = this.props;

         return ($('div', { className: 'comment' },
             $('h2', {}, 'Label'),
             $('input', {
                 type: 'text',
                 value: commentary,
                 onChange: handleChange
             })
         ));
     }
 }

 Comment.PropTypes = {
     commentary: string,
     handleChange: func
 };

 export default Comment;

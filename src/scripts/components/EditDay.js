 import React from 'react';

 const {
     string,
     func,
     object
 } = React.PropTypes;

 class EditDay extends React.Component {

     constructor(props) {

         super(props);
         const maxlen = 128;

         this.state = {
             maxlength: maxlen
         };

         this.handleChange = this.handleChange.bind(this);
     }

     shouldComponentUpdate(nextProps, nextState) {
         return this.props.display !== nextProps.display ||
             this.state.len !== nextState.len;
     }

     componentWillReceiveProps(nextProps) {

         if (this.props.editDayId.id !== nextProps.editDayId.id) {

             const { maxlength } = this.state;

             this.setState({
                 commentary: '',
                 len: 0,
                 counter: `0/${maxlength}`
             });
         }
     }

     componentDidMount() {
         this.textInput.focus();
     }

     handleChange(evt) {

         const maxLen = this.state.maxlength;
         const len = evt.target.value.length || 0;

         evt.preventDefault();

         if (len > maxLen) {
             return;
         }

         this.setState({
             len,
             commentary: evt.target.value,
             counter: `${len}/${maxLen}`
         });
     }

     render() {

         const $ = React.createElement;
         const {
             display,
             handleCancel,
             handleSave
         } = this.props;
         const { maxlength, counter, commentary } = this.state;
         const inputRef = ($elm) => {
             this.textInput = $elm;
         };

         const getInputValue = () => {
             const { id, period } = this.props.editDayId;
             return ({
                 id,
                 [period]: this.textInput.value
             });
         };

         return (

             $('div', { style: { display } },

                 $('h2', {}, 'Label'),

                 $('input', {
                     type: 'text',
                     maxLength: maxlength,
                     placeholder: 'edit week day',
                     value: commentary,
                     onChange: this.handleChange,
                     ref: inputRef
                 }),

                 $('p', {}, counter),

                 $('nav', {},

                     $('button', {
                         type: 'button',
                         onClick: handleSave(getInputValue)
                     }, 'Save'),

                     $('button', {
                         type: 'button',
                         onClick: handleCancel
                     }, 'Delete')
                 )
             )
         );
     }
 }

 EditDay.PropTypes = {
     display: string,
     comment: string.isRequired,
     editDayId: object.isRequired,
     handleCancel: func,
     handleSave: func
 };

 export default EditDay;

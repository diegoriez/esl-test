export default React => {

    const $ = React.createElement;

    const {
        string,
        array
    } = React.PropTypes;

    const comp = ({ title, data, className }) =>
        ($('div', { className },
            $('h2', {}, title),
            $('select', { name: 'first-day-selection' },

                data.map((item, idx) =>
                    $('option', { key: `fds${idx}`, value: item.id }, item.label))

            )));

    comp.PropTypes = {
        title: string.isRequired,
        data: array.isRequired,
        className: string
    };

    return comp;

};

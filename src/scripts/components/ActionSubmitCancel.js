import React from 'react';

class ActionSubmitCancel extends React.Component {

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const $ = React.createElement;
        return ($('nav', { className: 'submit-schedule' },
            $('button', { id: 'save-schedule' }, 'Save'),
            $('button', { id: 'Cancel-schedule' }, 'Cancel'))
        );
    }
}


export default ActionSubmitCancel;

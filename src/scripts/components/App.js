import createWeekStructurePlanningSelector from 'scripts/containers/week-structure-planning';
import createComment from 'scripts/containers/comment';
import createWeekPlannerTable from 'scripts/containers/week-planner-table';
import createActionSubmitCancel from 'scripts/containers/action-submit-cancel';
import createEditDay from 'scripts/containers/edit-day';

export default React => {

    const $ = React.createElement;
    const ActionSubmitCancel = createActionSubmitCancel;
    const Comment = createComment;
    const EditDay = createEditDay;
    const WeekPlannerTable = createWeekPlannerTable(React);
    const WeekStructurePlanningSelector = createWeekStructurePlanningSelector(React);

    const App = () => (

        $('div', { className: 'edit-school-week' },

            $('h1', {}, 'Week planning'),

            $(WeekStructurePlanningSelector),

            $(Comment),

            $(EditDay),

            $(WeekPlannerTable),

            $('footer', {},
                $(ActionSubmitCancel)
            )
        )
    );

    return App;
};

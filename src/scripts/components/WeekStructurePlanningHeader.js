import { prop } from 'ramda';
import WeekStructurePlanningSelector from 'scripts/components/WeekStructurePlanningSelector';

export default React => {

    const $ = React.createElement;

    const {
        array
    } = React.PropTypes;

    const weekStructureComp = WeekStructurePlanningSelector(React);

    const comp = ({ data }) =>
        ($('header', { className: 'week-structure-planning' },


        [
                { key: 'k_0', data: prop('daysList', data), title: 'First Day' },
                { key: 'k_1', data: prop('numberOfDays', data), title: 'Number of Days' },
                { key: 'k_2', data: prop('numberOfPeriods', data), title: 'Number of Periods' },
                { key: 'k_3', data: prop('beginningOfPeriod', data), title: 'Beginning of Period' }
        ]
            .map((value) => $(weekStructureComp, value))

        ));

    comp.PropTypes = {
        data: array.isRequired
    };

    return comp;

};

/* eslint-disable no-underscore-dangle */
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import createApp from 'scripts/components/App';
import { rootReducer } from 'scripts/reducers';
import {
    $qs,
    fetchWeekStructureData,
    fetchWeekDaysDataPlanning,
    weekDataPlanningDelegator
} from 'scripts/helpers';

(() => {

    const initialState = {
        weekStruturePlanning: {
            'daysList': [],
            'numberOfDays': [],
            'numberOfPeriods': [],
            'beginningOfPeriod': []
        },
        comment: '',
        days: { entities: { days: {} }, result: [] },
        editDayId: { id: null, period: null }
    };

    const store = createStore(
        rootReducer,
        initialState,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    const $ = React.createElement;
    const App = createApp(React);

    render(
        $(Provider, { store }, $(App)),
        $qs('#root')
    );

    fetchWeekStructureData(store.dispatch);
    fetchWeekDaysDataPlanning(store.dispatch);
    weekDataPlanningDelegator(store.dispatch);

})();

const deepFreeze = require('deep-freeze');

const weekStruturePlanning = deepFreeze({
    'data': {
        'type': 'week-structure-planning',
        'id': '1',
        'daysList': [{
            'label': 'sunday',
            'id': '1'
        }, {
            'label': 'monday',
            'id': '2'
        }, {
            'label': 'tuesday',
            'id': '3'
        }, {
            'label': 'wensday',
            'id': '4'
        }, {
            'label': 'thursday',
            'id': '5'
        }, {
            'label': 'friday',
            'id': '6'
        }, {
            'label': 'saturday',
            'id': '7'
        }],
        'numberOfDays': [
            { 'id': '1', 'label': '1' },
            { 'id': '2', 'label': '2' },
            { 'id': '3', 'label': '3' },
            { 'id': '4', 'label': '4' },
            { 'id': '5', 'label': '5' },
            { 'id': '6', 'label': '6' },
            { 'id': '7', 'label': '7' }
        ],
        'numberOfPeriods': [
            { 'id': '1', 'label': '1' },
            { 'id': '2', 'label': '2' },
            { 'id': '3', 'label': '3' }
        ],
        'beginningOfPeriod': [
            { 'id': '1', 'label': 'moorning' },
            { 'id': '2', 'label': 'afternoon' },
            { 'id': '3', 'label': 'evening' }
        ]
    }
});

const weekPlanDataJSON = deepFreeze({
    'data': {
        'type': 'week-schedule',
        'id': '1',
        'attributes': {
            'startDay': '1',
            'endDay': '7',
            'numberOfDays': '7',
            'created': '2015-05-22T14:56:29.000Z',
            'updated': '2015-05-22T14:56:28.000Z'
        },
        'comment': 'Lorem ipsum dolor.',
        'days': [{
            'type': 'label-planing',
            'id': '1',
            'label': 'sunday',
            'moorning': 'Arraival Welcome',
            'afternoon': 'Arraival Welcome',
            'evening': 'Welcome Party',
            'fullday': ''
        }, {
            'type': 'label-planing',
            'id': '2',
            'label': 'monday',
            'moorning': 'Classes',
            'afternoon': 'Sports',
            'evening': 'Film nigth',
            'fullday': ''
        }, {
            'type': 'label-planing',
            'id': '3',
            'label': 'tuesday',
            'moorning': 'Classes',
            'afternoon': 'Sports',
            'evening': 'Film nigth',
            'fullday': ''
        }, {
            'type': 'label-planing',
            'id': '4',
            'label': 'wensday',
            'moorning': 'Classes',
            'afternoon': 'Sports',
            'evening': 'Film nigth',
            'fullday': ''

        }, {
            'type': 'label-planing',
            'id': '5',
            'label': 'thursday',
            'moorning': 'Classes',
            'afternoon': 'Sports',
            'evening': 'Film nigth',
            'fullday': ''
        }, {
            'type': 'label-planing',
            'id': '6',
            'label': 'friday',
            'moorning': 'Classes',
            'afternoon': 'Sports',
            'evening': 'Film nigth',
            'fullday': ''
        }, {
            'type': 'label-planing',
            'id': '7',
            'label': 'saturday',
            'moorning': 'A',
            'afternoon': 'B',
            'evening': 'C',
            'fullday': 'Graduation'
        }]
    }
});

const daysListSchema = deepFreeze({
    entities: {
        days: {
            1: { afternoon: 'Arraival Welcome', evening: 'Welcome Party', fullday: '', id: '1', label: 'sunday', moorning: 'Arraival Welcome', type: 'label-planing' },
            2: { afternoon: 'Sports', evening: 'Film nigth', fullday: '', id: '2', label: 'monday', moorning: 'Classes', type: 'label-planing' },
            3: { afternoon: 'Sports', evening: 'Film nigth', fullday: '', id: '3', label: 'tuesday', moorning: 'Classes', type: 'label-planing' },
            4: { afternoon: 'Sports', evening: 'Film nigth', fullday: '', id: '4', label: 'wensday', moorning: 'Classes', type: 'label-planing' },
            5: { afternoon: 'Sports', evening: 'Film nigth', fullday: '', id: '5', label: 'thursday', moorning: 'Classes', type: 'label-planing' },
            6: { afternoon: 'Sports', evening: 'Film nigth', fullday: '', id: '6', label: 'friday', moorning: 'Classes', type: 'label-planing' },
            7: { afternoon: 'B', evening: 'C', fullday: 'Graduation', id: '7', label: 'saturday', moorning: 'A', type: 'label-planing' }
        }
    },
    result: ['1', '2', '3', '4', '5', '6', '7']
});

const model = deepFreeze({
    weekStruturePlanning: weekStruturePlanning.data,
    startDay: 1,
    endDay: 7,
    numberOfPeriods: 3,
    numberOfDays: 7,
    comment: weekPlanDataJSON.data.comment,
    updatedDaysIds: [],
    days: daysListSchema,
    editDayId: { id: '3', period: 'moorning' }
});


export const getWeekPlanData = () => weekPlanDataJSON;
export const getweekStruturePlanning = () => weekStruturePlanning;
export const getDayList = () => weekStruturePlanning.data.daysList;
export const getModel = () => model;
export const getDayListSchema = () => daysListSchema;


export const getWeekDayToBeUpdated = () => ({
    moorning: 'lorem',
    evening: 'Lorem ipsum dolor sit amet, consectetur adipisicing.',
    id: '2'
});

export const getWeeDataUpdated = () => {

    const aux = JSON.parse(JSON.stringify(daysListSchema));
    aux.entities.days[2] = Object.assign(aux.entities.days[2], {
        moorning: 'lorem',
        evening: 'Lorem ipsum dolor sit amet, consectetur adipisicing.',
        id: '2'
    });

    return deepFreeze(aux);
};

export const getWeeDayDeleted = () => {

    const aux = JSON.parse(JSON.stringify(daysListSchema));
    aux.entities.days[2] = {};

    return deepFreeze(aux);
};

// export const getWeekDataToBeUpdated = () => ({
//     1: { moorning: 'Lorem ipsum.', afternoon: 'Lorem ipsum dolor.', id: '1' },
//     2: { moorning: 'lorem', evening: 'Lorem ipsum dolor sit amet, consectetur adipisicing.', id: '2' }
// });
// export const getDaysListSchemaUpdated = () => {

//     const cloned = JSON.parse(JSON.stringify(daysListSchema));
//     cloned.entities.days[1] = Object.assign(cloned.entities.days[1], getWeekDataToBeUpdated()[1]);
//     cloned.entities.days[2] = Object.assign(cloned.entities.days[2], getWeekDataToBeUpdated()[2]);

//     return deepFreeze(cloned);

// };

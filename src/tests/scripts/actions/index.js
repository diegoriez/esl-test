import test from 'tape';
import {
    getWeekPlanData,
    getweekStruturePlanning,
    getWeekDayToBeUpdated
} from 'tests/fixtures';
import {
    weekStructureDataReceived,
    weekPlanDataReceived,
    weekDayUpdated,
    weekDayDeleted,
    commentUpdated,
    editDay
} from 'scripts/actions';

const before = test;
const after = test;

before('description: actions creators', (t) => {
    t.end();
});

test('weekStructureDataReceived', (t) => {

    const actual = weekStructureDataReceived(getweekStruturePlanning().data);
    const expect = {
        type: 'week.structure.planning.received',
        payload: getweekStruturePlanning().data
    };

    t.deepEqual(actual, expect);
    t.end();

});

test('weekPlanDataReceived', (t) => {

    const actual = weekPlanDataReceived(getWeekPlanData().data.days);
    const expect = {
        type: 'week.plan.data.received',
        payload: getWeekPlanData().data.days
    };

    t.deepEqual(actual, expect);
    t.end();
});

test('commentUpdated', (t) => {

    const actual = commentUpdated(getWeekPlanData().data.comment);
    const expect = {
        type: 'comment.updated',
        payload: getWeekPlanData().data.comment
    };

    t.deepEqual(actual, expect);
    t.end();
});

test('weekDayUpdated', (t) => {

    const actual = weekDayUpdated(getWeekDayToBeUpdated());
    const expect = {
        type: 'week.day.updated',
        payload: getWeekDayToBeUpdated()
    };

    t.deepEqual(actual, expect);
    t.end();
});

test('weekDayDelete', (t) => {

    const actual = weekDayDeleted(2);
    const expect = {
        type: 'delete.week.day',
        payload: 2
    };

    t.deepEqual(actual, expect);
    t.end();
});

test('editDay', (t) => {

    const actual = editDay({ id: '3', period: 'moorning' });
    const expect = {
        type: 'edit.day',
        payload: { id: '3', period: 'moorning' }
    };

    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', (t) => {
    t.end();
});

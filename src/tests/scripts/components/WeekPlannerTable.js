import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';

import { getDayListSchema, getweekStruturePlanning } from 'tests/fixtures';
import component from 'scripts/components/WeekPlannerTable';

const $ = React.createElement;
const Comp = component(React);

const before = test;
const after = test;

before('description: WeekPlannerTable', (t) => {
    t.end();
});

test('basic table with no data', (t) => {

    const msg = 'should render correctly';

    const actual = shallow($(Comp)).type();
    const expect = 'table';

    t.equal(actual, expect, msg);
    t.end();
});

test('starting day is on Sunday and end day is on Saturday', (t) => {

    const msg = 'should have 8 elements in the head';

    const props = {
        days: getDayListSchema().entities.days,
        result: getDayListSchema().result
    };

    const actual = shallow($(Comp, props)).find('thead').childAt(0).children().length;
    const expect = 8;

    t.equal(actual, expect, msg);
    t.end();

});

test('its has 3 periods', (t) => {

    const msg = 'should have 3 elements in the tbody';

    const props = {
        periods: getweekStruturePlanning().data.beginningOfPeriod
    };

    const actual = shallow($(Comp, props)).find('tbody').children().length;
    const expect = 3;

    t.equal(actual, expect, msg);
    t.end();
});

test('each period has 8 items', (t) => {

    const msg = 'should have 8 per period';

    const props = {
        days: getDayListSchema().entities.days,
        result: getDayListSchema().result,
        periods: getweekStruturePlanning().data.beginningOfPeriod
    };

    const actual = shallow($(Comp, props)).find('tbody').childAt(0).children().length;
    const expect = 8;

    t.equal(actual, expect, msg);
    t.end();
});

after('end test ---------------------------------------', (t) => {
    t.end();
});

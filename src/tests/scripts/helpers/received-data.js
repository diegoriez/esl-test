import test from 'tape';
import { normalizeWeekDaysData } from 'scripts/helpers/received-data';
import { getWeekPlanData, getDayListSchema } from 'tests/fixtures';

const before = test;
const after = test;

before('description: helpers received data', (t) => {
    t.end();
});

test('normalizeWeekDaysData', (t) => {

    const days = getWeekPlanData().data.days;
    const actual = normalizeWeekDaysData(days);
    const expect = getDayListSchema();

    t.deepEqual(actual, expect);
    t.end();
});

after('end test ---------------------------------------', (t) => {
    t.end();
});

import test from 'tape';
import {
    weekStruturePlanning,
    comment,
    daysDataUpdated,
    updatedDaysIds,
    editDayId
} from 'scripts/reducers';
import {
    getweekStruturePlanning,
    getDayListSchema,
    getWeekDayToBeUpdated,
    getWeeDataUpdated,
    getWeeDayDeleted,
    getModel
} from 'tests/fixtures';

const before = test;
const after = test;

before('description: actions reducers', (t) => {
    t.end();
});

test('weekStruturePlanning', (t) => {

    const actual = weekStruturePlanning({}, {
        type: 'week.structure.planning.received',
        payload: getweekStruturePlanning().data
    });
    const expect = getweekStruturePlanning().data;

    t.deepEqual(actual, expect);
    t.end();
});

test('comment', (t) => {

    const actual = comment({}, {
        type: 'comment.updated',
        payload: 'Lorem ipsum dolor sit amet, consectetur adipisicing.'
    });
    const expect = 'Lorem ipsum dolor sit amet, consectetur adipisicing.';

    t.deepEqual(actual, expect);
    t.end();
});

test('daysDataUpdated', (t) => {

    let actual, expect, msg;

    msg = 'The weekly plan data was received and updated';

    actual = daysDataUpdated({}, {
        type: 'week.plan.data.received',
        payload: getDayListSchema()
    });
    expect = getDayListSchema();
    t.deepEqual(actual, expect, msg);

    msg = 'The data of the day were modified';
    actual = daysDataUpdated(getModel().days, {
        type: 'week.day.updated',
        payload: getWeekDayToBeUpdated()
    });
    expect = getWeeDataUpdated();
    t.deepEqual(actual, expect, msg);

    msg = 'The data of the day were deleted';
    actual = daysDataUpdated(getModel().days, {
        type: 'delete.week.day',
        payload: 2
    });
    expect = getWeeDayDeleted();
    t.deepEqual(actual, expect, msg);

    t.end();
});

test('updatedDaysIds', (t) => {

    let actual, expect;

    actual = updatedDaysIds([], {
        type: 'week.day.updated',
        payload: getWeekDayToBeUpdated()
    });

    expect = ['2'];
    t.deepEqual(actual, expect);

    actual = updatedDaysIds(['2'], {
        type: 'delete.week.day',
        payload: '5'
    });

    expect = ['2', '5'];
    t.deepEqual(actual, expect);


    t.end();

});

test('editDayId', (t) => {

    let actual, expect;

    actual = editDayId('', {
        type: 'edit.day',
        payload: { id: '3', period: 'moorning' }
    });

    expect = { id: '3', period: 'moorning' };
    t.deepEqual(actual, expect);


    actual = editDayId({ id: '3', period: 'moorning' }, {
        type: 'week.day.updated',
        payload: { id: '', period: '' }
    });

    expect = { id: null, period: null };

    t.deepEqual(actual, expect);

    t.end();

});

after('end test ---------------------------------------', (t) => {
    t.end();
});
